#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void PrintString(string text) {
	string filepath = "test.txt";
	ofstream ofs(filepath);
	
	//display output of madlib on the txt
	ofs << text;
	ofs.close();
	
}

string* madlibdisplay(string *input){
	string* madlib = new string;
	*madlib += "\nA vacation is when you take a trip to some " +
		input[0] + " place with your " +
		input[1] + " family. \nUsually you go to some place that is near a/an " +
		input[2] + " or up on a/an " +
		input[3] + " A good \nvacation place is one where you can ride " +
		input[4] + " or play " +
		input[5] + " or go hunting for " +
		input[6] + ". \nI like to spend my time " +
		input[7] + " or " +
		input[8] + " When parents go on a vacation, they spend their time eating three \n" +
		input[9] + " a day, and fathers play golf, and mothers sit around " +
		input[10] + ". Last summer, my little brother fell\nin a/an " +
		input[11] + " and got posion " +
		input[12] + " all over his " +
		input[13] + " My family is going to go to (the) " +
		input[14] + ",\nand I will practice " +
		input[15] + ". Parents need vacations more than kids because parents are always very " +
		input[16] + "\nand because they have to work " +
		input[17] + " hours every day all year making enough " +
		input[18] + " to pay for the vacation.";
	
	return madlib;
};
int main() {
		
		string answ;

		//declarations
		const int words = 19;

		//arrays
		string input[words]; //
		/**
			string strings[22]{
			"A vacation is when you take a trip to some ",
			" place with your ",
			" family. ",
			" Usually you go to some place that is near a/an ",
			" or up on a/an ",
			"A good vacation place is one where you can ride ",
			" or play ",
			" or go hunting for ",
			" I like to spend my time ",
			" or ",
			" When parents go on a vacation, they spend their time eating three ",
			" a day, and fathers play golf, and mothers sit around ",
			" Last summer, my little brother fell in a/an ",
			" and got posion ",
			" all over his ",
			" My family is going to go to (the) ",
			", and I will practice ",
			". Parents need vacations more than kids because parents are always very ",
			" and because they have to work ",
			" hours every day all year making enough ",
			" to pay for the vacation."
		}; **/
		string INSERT[words] = {
			"ADJECTIVE: ",
			"ADJECTIVE: ",
			"NOUN: ",
			"NOUN: ",
			"PLURAL NOUN: ",
			"GAME: ",
			"PLURAL NOUN: ",
			"VERB ENDING IN 'ING': ",
			"VERB ENDING IN 'ING': ",
			"PLURAL NOUN: ",
			"VERB ENDING IN 'ING': ",
			"NOUN: ",
			"PLANT: ",
			"PART OF THE BODY: ",
			"A PLACE: ",
			"VERB ENDING IN 'ING': ",
			"ADJECTIVE: ",
			"NUMBER: ",
			"PLURAL NOUN: "

		};

		//print insert & have user cin input
		for (int i = 0; i < words; i++) {
			cout << INSERT[i];
			cin >> input[i];
		}

		//print strings & input
		string *display = madlibdisplay(input);

		//to display result of input
		cout << *display;
		cout << "\n";

			cout << "Would you like to save output to file? (y/n): ";
			cin >> answ;

			//if, else statement, anything besides 'y' will not save file
			if (answ == "y") {
				PrintString(*display);
				cout << "File has been saved.";
			} 
			else {
				cout << "File not saved.";
			}

	
	(void)_getch();
	return 0;
}